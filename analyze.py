#!/usr/bin/env python

import os
import sys
import json
import dateutil.parser
from optparse import OptionParser

import colorsys

from scipy import stats
import scipy as sp
import heatmap
from collections import namedtuple
from scipy.cluster.vq import kmeans2
import numpy as np
import kml_movement

from urlparse import urlparse

from plot_helper import PlotHelper
from histogram_analysis import HistogramAnalysis


class DataParser:

    def __init__(self, data):
        self.data = data

    def extract(self, fn):
        return [fn(d) for d in self.data]

    def get(self, name):
        if not name:
            return None
        if hasattr(self, "get_" + name):
            return getattr(self, "get_" + name)()
        else:
            return self.extract(lambda d: d[name])

    def get_module(self, statname):
        if hasattr(stats, statname):
            return getattr(stats, statname)
        elif hasattr(sp, statname):
            return getattr(sp, statname)
        elif hasattr(np, statname):
            return getattr(np, statname)
        else:
            raise NotImplementedError('Stat not available')

    def stats(self, statname, name):
        d = self.get(name)
        return self.get_module(statname)(d)

    def stats2(self, statname, n1, n2):
        d1 = self.get(n1)
        d2 = self.get(n2)
        return self.get_module(statname)(d1, d2)

    def get_locations(self):
        return self.extract(lambda d: (d['lng'], d['lat']))

    def get_datetime(self):
        return self.extract(lambda d: dateutil.parser.parse(d['datetime']))

    def get_separate_rgb(self):
        r, g, b = [], [], []

        def f(d):
            c = d['avg_color']
            r.append((c >> 16) & 0xFF)
            g.append((c >> 8) & 0xFF)
            b.append(c & 0xFF)

        self.extract(f)
        return [r, g, b]

    def get_raw_data(self):
        return self.data

    def __str__(self):
        return self.data


class Analyzer:

    def __init__(self, filenames):
        d = []
        for fname in filenames:
            d += json.load(open(fname))

        self.data = DataParser(d)

    def stress_vs_color(self):
        colors = self.data.get_separate_rgb()
        stresses = [x * 100 for x in self.data.get('stress')]

        ph = PlotHelper()
        cnames = ["Red", "Green", "Blue"]
        for color in colors:
            ph.addSeries(range(len(color)), color, label=cnames[0])
            cnames = cnames[1:]
        ph.addSeries(range(len(stresses)), stresses, label="Stress")
        ph.plot()

    def stress_vs_avg_rgb(self):

        fs = dict()

        def f(d):
            s = d['stress']
            c = d['avg_color']
            r = ((c >> 16) & 0xFF)
            g = ((c >> 8) & 0xFF)
            b = (c & 0xFF)

            if s in fs:
                fs[s]['r'].append(r)
                fs[s]['g'].append(g)
                fs[s]['b'].append(b)
            else:
                fs[s] = {
                    'r': [r],
                    'g': [g],
                    'b': [b]
                }

        self.data.extract(f)

        ph = PlotHelper()

        avg_rs = []
        avg_gs = []
        avg_bs = []
        stresses = sorted(fs.iterkeys())

        for key in sorted(fs.iterkeys()):
            avg_rs.append(sp.mean(fs[key]['r']))
            avg_gs.append(sp.mean(fs[key]['g']))
            avg_bs.append(sp.mean(fs[key]['b']))

        ph.addSeries(stresses, avg_rs, label="Avg. Red")
        ph.addSeries(stresses, avg_gs, label="Avg. Green")
        ph.addSeries(stresses, avg_bs, label="Avg. Blue")

        ph.set_xy_labels("Stress", "Average Color")

        ph.plot()

    def stress_vs_hls(self):

        fs = dict()

        def f(d):
            stress = d['stress']
            c = d['avg_color']
            r = ((c >> 16) & 0xFF)
            g = ((c >> 8) & 0xFF)
            b = (c & 0xFF)

            hsv = colorsys.rgb_to_hsv(r, g, b)
            h = hsv[0]
            s = hsv[1]
            v = hsv[2]

            # print (r, g, b)
            print hsv

            if stress in fs:
                fs[stress]['h'].append(h * 100)
                fs[stress]['s'].append(s * 100)
                fs[stress]['v'].append(v)
            else:
                fs[stress] = {
                    'h': [h],
                    's': [s],
                    'v': [v]
                }

        self.data.extract(f)

        ph = PlotHelper()

        avg_hs = []
        avg_ss = []
        avg_vs = []
        stresses = sorted(fs.iterkeys())

        for key in sorted(fs.iterkeys()):
            avg_hs.append(sp.mean(fs[key]['h']))
            avg_ss.append(sp.mean(fs[key]['s']))
            avg_vs.append(sp.mean(fs[key]['v']))

        ph.addSeries(stresses, avg_hs, label="Avg. Hue")
        ph.addSeries(stresses, avg_ss, label="Avg. Saturation")
        ph.addSeries(stresses, avg_vs, label="Avg. Value")

        ph.set_xy_labels("Stress", "Average HSV")

        ph.plot()

    def cluster(self):

        stresses = []
        locs = []
        ids = []

        def f(d):
            s = d['stress']
            lat = d['lat']
            lng = d['lng']
            stresses.append(s)
            locs.append([lat, lng])
            ids.append(d['id'])

        self.data.extract(f)

        # whitened = whiten(np.array(locs))
        centroids, labels = kmeans2(np.array(locs), 4)
        print centroids
        print labels

        lab_dict = dict()
        nk = 0
        for p in ids:
            lab_dict[p] = labels[nk]
            nk += 1

        fs = dict()

        def f(d):
            s = d['stress']
            p = d['imgURI']

            fname = p.split('/')[-1]

            fs[fname] = {
                'stress': s,
                'centroid': lab_dict[d['id']]
            }

        self.data.extract(f)

        ha = HistogramAnalysis("color_pics", fs)

    def stress_heatmap(self):
        stresses = self.data.get('stress')
        locs = self.data.get('locations')

        hm = heatmap.Heatmap()
        hm.heatmap(locs, "classic.png", dotsize=stresses)
        hm.saveKML("most_visited.kml")
        hm.saveKMZ("most_visited")

        times = self.data.get('datetime')
        LocTuple = namedtuple('LocTuple', ['lat', 'long', 'time'])
        movement_data = []

        i = 0
        for time in times:
            movement_data.append(
                LocTuple(locs[i][0],
                         locs[i][1],
                         time
                         )
            )
            i += 1
        kml_movement.main(movement_data)

    def filenames_to_stress(self):

        fs = dict()

        def f(d):
            s = d['stress']
            p = d['imgURI']

            fname = p.split('/')[-1]

            fs[fname] = {
                'stress': s,
                'centroid': None
            }

        self.data.extract(f)

        ha = HistogramAnalysis("color_pics", fs)

    def generate(self):
        # print self.data.stats('mean', 'id')
        # print self.data.stats2('corrcoef', 'avg_color', 'stress')
        # self.stress_vs_color()
        # self.stress_vs_avg_rgb()
        # self.stress_vs_hls()
        # self.cluster()
        # self.stress_heatmap()
        self.filenames_to_stress()


def main():
    usage = "%prog [exported_data.json]"
    description = "Takes exported json file from ColorApp " \
                  "and generates data visualizations and statistics."

    parser = OptionParser(usage="%s\n\n%s" % (usage, description))
    (args) = parser.parse_args()
    path = os.path.abspath(os.path.join(sys.argv[0], os.path.pardir))
    os.chdir(path)

    viz = Analyzer(args[1])
    viz.generate()

if __name__ == "__main__":
    main()
