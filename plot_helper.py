
import matplotlib as mpl
from matplotlib import pyplot as plt

NUM_FIGURES = 1


class PlotHelper:

    def __init__(self):
        global NUM_FIGURES
        plt.figure(NUM_FIGURES)
        NUM_FIGURES += 1
        self.xlabel = None
        self.ylabel = None
        self.yrange = None
        self.data = [[]]

    def addSeries(self, x, y, plt=0, label=None):
        if plt > len(self.data):
            raise Exception('Too hight a subplot')
        if len(self.data) < plt + 1:
            self.data.append([])
        self.data[plt].append({
            'data': [x, y],
            'label': label
        })

    def set_xy_labels(self, xlabel, ylabel):
        self.xlabel = xlabel
        self.ylabel = ylabel

    def set_y_range(self, yrange):
        self.yrange = yrange

    def plot(self):
        sp_num = 1
        for p_data in self.data:
            colors = ['r', 'g', 'b', 'k', 'm', 'y', 'c']
            ax = plt.subplot(len(self.data), 1, sp_num)
            sp_num += 1

            for l_data in p_data:
                x = l_data['data'][0]
                y = l_data['data'][1]
                ax.plot(x, y, colors[0], label=l_data['label'])
                if self.yrange:
                    ax.axis([0, 255, 0, self.yrange])
                colors = colors[1:]

            ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                      ncol=2, mode="expand", borderaxespad=0.)

            ax.set_xlabel(self.xlabel)
            ax.set_ylabel(self.ylabel)

        plt.grid(True)
        plt.tight_layout()
        plt.show()
